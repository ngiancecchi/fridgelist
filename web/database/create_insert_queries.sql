-- phpMyAdmin SQL Dump
-- version 2.8.0.2
-- http://www.phpmyadmin.net
-- 
-- Host: sql.giancecchi.com
-- Generation Time: Apr 04, 2015 at 10:15 AM
-- Server version: 5.1.63
-- PHP Version: 4.3.10-22
-- 
-- Database: `giancecc75434`
-- 
CREATE DATABASE `giancecc75434` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `giancecc75434`;

-- --------------------------------------------------------

-- 
-- Table structure for table `product`
-- 

CREATE TABLE `product` (
  `idProduct` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `image` text,
  PRIMARY KEY (`idProduct`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- 
-- Dumping data for table `product`
-- 

INSERT INTO `product` (`idProduct`, `name`, `image`) VALUES (1, 'Fruit Juice', 'http://nicola.giancecchi.com/dev/fridgelist/assets/fruitjuice.png'),
(2, 'Eggs', 'http://nicola.giancecchi.com/dev/fridgelist/assets/eggs.png'),
(3, 'Biscuits', 'http://nicola.giancecchi.com/dev/fridgelist/assets/biscuits.png'),
(4, 'Tomatoes', 'http://nicola.giancecchi.com/dev/fridgelist/assets/tomatoes.png'),
(5, 'Snacks', 'http://nicola.giancecchi.com/dev/fridgelist/assets/snacks.png'),
(6, 'Pasta', 'http://nicola.giancecchi.com/dev/fridgelist/assets/pasta.png');

-- --------------------------------------------------------

-- 
-- Table structure for table `tag`
-- 

CREATE TABLE `tag` (
  `idTag` varchar(255) NOT NULL,
  `idProduct` int(11) NOT NULL,
  PRIMARY KEY (`idTag`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `tag`
-- 

INSERT INTO `tag` (`idTag`, `idProduct`) VALUES ('ecd4d2ab', 5),
('6d22876e', 4),
('33629e75', 3),
('09d8dfda', 2),
('09d7183a', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `transaction`
-- 

CREATE TABLE `transaction` (
  `idTransaction` int(11) NOT NULL AUTO_INCREMENT,
  `idTag` varchar(255) NOT NULL,
  `date` bigint(20) NOT NULL,
  PRIMARY KEY (`idTransaction`),
  UNIQUE KEY `idTag` (`idTag`)
) ENGINE=MyISAM AUTO_INCREMENT=221 DEFAULT CHARSET=latin1 AUTO_INCREMENT=221 ;

-- 
-- Dumping data for table `transaction`
-- 

INSERT INTO `transaction` (`idTransaction`, `idTag`, `date`) VALUES (218, '33629e75', 1428135164),
(219, 'ecd4d2ab', 1428135173),
(220, '09d8dfda', 1428135181);
