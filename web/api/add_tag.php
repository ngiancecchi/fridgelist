<?php
    
    include_once "db_access.php";
    
    class UseTag extends DatabaseClass{
        
        function __construct() {
            parent::__construct();
        }
        
        function __destruct() {
            parent::__destruct();
        }
        
        function useTag(){
            
            
            if (isset($_GET["tid"])) {
                
                $idTag = $_GET["tid"];
                
                /* Checks if the passed tag id already exists in database */
                
                $stmt = $this->db->prepare("SELECT idTag FROM tag WHERE idTag = ?");
                $stmt->bind_param('s', $idTag);
                $stmt->execute();
                $stmt->bind_result($idTag);
                $array = array();
                $found = false;
                if ($stmt->fetch()) {
                    //do stuff with the data
                    $found = true;
                }
                $stmt->close();
                
                /* If id not found, return 0 */
                if(!$found) {
                    echo '{0}';
                    return false;
                }
                
                /* Check if a tag is already in list */
                $stmt = $this->db->prepare("SELECT idTag FROM transaction WHERE idTag = ?");
                $stmt->bind_param('s', $idTag);
                $stmt->execute();
                $stmt->bind_result($idTag);
                $array = array();
                $already_exist = false;
                if ($stmt->fetch()) {
                    //do stuff with the data
                    $already_exist = true;
                }
                $stmt->close();
                
                /* If found, return 2 */
                if($already_exist) {
                    echo '{2}';
                    return true;
                }
                
                /* Else proceed the tag id */
                $stmt = $this->db->prepare("INSERT INTO `transaction` (`idTag`, `date`) VALUES (?, UNIX_TIMESTAMP())");
                $stmt->bind_param('s', $idTag);
                $stmt->execute();
                
                if ($stmt->errno) {
                    
                    $stmt->close();
                    echo '{0}';
                    
                    return false;
                }
                
                $stmt->close();
                echo '{1}';
                
                return true;
            }
            
            echo '{0}';
            
            return false;
            
        }
    }
    
    $tagholder = new UseTag;
    $tagholder->useTag();
    
    ?>