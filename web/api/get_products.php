<?php

 	include_once "db_access.php";
 	
class GetProducts extends DatabaseClass {
    
    function __construct() {
        parent::__construct();
    }
    
    function __destruct() {
        parent::__destruct();
    }
 	function fetchProducts(){
		
		
        $stmt = $this->db->prepare("SELECT transaction.idTag AS idTag, product.name AS name, transaction.date AS date, image FROM ((transaction INNER JOIN tag ON transaction.idTag = tag.idTag) INNER JOIN product ON tag.idProduct = product.idProduct)");
        $stmt->execute();
       $stmt->bind_result($idTag, $name, $date, $image);
	   $array = array();
       while ($stmt->fetch()) {
           array_push($array, array("idTag"=>$idTag,"name"=>$name,"date"=>$date,"image"=>$image));
       }
       $stmt->close();
		echo json_encode($array);
		return true;

	}
}

$prods = new GetProducts;
$prods->fetchProducts();

?>