<?php
    
    include_once "db_access.php";
    
    class Reset extends DatabaseClass {
        
        
        function __construct() {
            parent::__construct();
        }
        
        function __destruct() {
            parent::__destruct();
        }
        
        function reset(){
            
            $stmt = $this->db->prepare("DELETE FROM transaction");
            $stmt->execute();
            
            if ($stmt->errno) {
                
                $stmt->close();
                echo "{0}";
                return false;
            }
            
            $stmt->close();
            echo "{1}";
            return true;
        }
    }
    
    $reset = new Reset;
    $reset->reset();
    
    ?>