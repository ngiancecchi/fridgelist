#ifndef ____WiFiSettings__
#define ____WiFiSettings__

#include "Defines.h"

class WiFiSettings {

private:
    String networkSSID;
    String password;
public:
    WiFiSettings();
    WiFiSettings(String networkSSID, String password);
    String getSSID();
    String getPassword();
};


#endif
