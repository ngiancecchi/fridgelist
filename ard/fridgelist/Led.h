#ifndef __LED__
#define __LED__

class Light {
public:
  virtual void switchOn() = 0;
  virtual void switchOff() = 0;    
};

class Led: public Light { 
public:
  Led(int pin, int colorLevel);
  void switchOn();
  void switchOff();    
private:
  int pin; 
  int colorLevel; 
};

#endif
