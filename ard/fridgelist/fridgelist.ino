#include <WiFi.h>
#include <SPI.h>
#include <EEPROM.h>
#include <Wire.h>
#include "NetworkManager.h"
#include "WiFiSettings.h"
#include "NFCManager.h"
#include "Defines.h"

NetworkManager* netman;
NFCManager* nfcman;
LEDManager ledman;

String netresp;
String tag;

boolean nfcres; 
boolean netconn;

boolean reading;

volatile boolean deleting;

void setup() {
  Serial.begin(115200);
 
  pinMode(3,INPUT);
  attachInterrupt(1, deleteCurrentList, FALLING);
  
  reading=false;
  
  ledman = LEDManager();
  ledman.setMulticolorLed(COLOR_YELLOW);
          
  nfcman = new NFCManager();
  nfcres = nfcman->setupBoard();
  
  netman = new NetworkManager(new WiFiSettings(NET_SSID,NET_PSW));
  netconn = netman->tryConnection();
  
  if(netconn && nfcres){
      ledman.setDefault();
  }else{
      ledman.setMulticolorLed(COLOR_RED);
  }
  
  deleting=false;
  
}

void deleteCurrentList(){
  noInterrupts();
  deleting=true;
#if defined(ISDEBUG)
  Serial.println("Interrupt called");
#endif
  interrupts();
}

void loop() {
  ledman.getScheduler().schedule();
  if(netconn && nfcres){
    if(!reading){
      tag = nfcman->readTag();
      if(!tag.equals("")){
        ledman.scheduleBlink(COLOR_YELLOW);
        boolean reqresult = netman->sendTag(tag);
        if(reqresult){
          reading=true;
        } else {
          ledman.setMulticolorLed(COLOR_RED);
          delay(DEFAULT_LED_DELAY);
          ledman.setDefault();
        }
      }
    }
    
    if(reading){
      netresp = netman->receiveResponse();
      if(!netresp.equals("")){
        ledman.manageServerResponse(netresp);
        #if defined(ISDEBUG)
        Serial.println(netresp);
        #endif
        reading=false;
        delay(DEFAULT_LED_DELAY);
        ledman.setDefault();
      }
    }
    
    if(deleting){
      ledman.scheduleBlink(COLOR_YELLOW);
      netman->deleteCurrentList();
      reading=true;
      deleting=false;
    }
  } 
}

/*
//freeRam function
//only for debug purposes
//Use with:

#if defined(ISDEBUG)
  Serial.println(freeRam());
#endif

//Function definition

int freeRam () 
{
  extern int __heap_start, *__brkval; 
  int v; 
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
}

*/

