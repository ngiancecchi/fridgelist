#include "WiFiSettings.h"

WiFiSettings::WiFiSettings(){
}

WiFiSettings::WiFiSettings(String netSSID, String pwd) {
    this->networkSSID = netSSID;
    this->password = pwd;
}
    
String WiFiSettings::getSSID(){
    return this->networkSSID;
}
    
String WiFiSettings::getPassword(){
    return this->password;
}
    
