#ifndef __MULTICOLORBLINKTASK__
#define __MULTICOLORBLINKTASK__

#include "Task.h"
#include "Led.h"

class MulticolorBlinkTask: public Task {

private:
  int pin[3];
  Light* led[3];
  int color[3];
  enum { ON, OFF} state;

public:
  MulticolorBlinkTask(int redPin, int greenPin, int bluePin);
  void setColorComponents(const int color[3]);
  void init(int period); 
  void tick();
};

#endif

