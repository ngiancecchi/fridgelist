#ifndef ____NFCManager__
#define ____NFCManager__
#include "Adafruit_PN532.h"
#include "Defines.h"

#define IRQ   (2)
#define RESET (3)

class NFCManager {
private:
  String hexCode (const byte * data, const uint32_t numBytes);
  uint8_t success;
  uint8_t uidLength;   // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
public:
  NFCManager();
  bool setupBoard();
  String readTag();
};


#endif
