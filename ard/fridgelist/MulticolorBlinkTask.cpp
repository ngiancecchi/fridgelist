#include "MulticolorBlinkTask.h"

MulticolorBlinkTask::MulticolorBlinkTask(int redPin, int greenPin, int bluePin){
  this->pin[0] = redPin;    
  this->pin[1] = greenPin;    
  this->pin[2] = bluePin; 
}
  
void MulticolorBlinkTask::setColorComponents(const int color[3]){
  for(int i = 0; i < 3; i++){
    this->color[i] = color[i];
  }
}
  
void MulticolorBlinkTask::init(int period){
  Task::init(period);
  for(int i = 0; i < 3; i++){
    led[i] = new Led(pin[i], color[i]); 
  }
  state = OFF;    
}
  
void MulticolorBlinkTask::tick(){
  switch (state){
    case OFF:
      for(int i = 0; i < 3; i++){
        led[i]->switchOn();
      }
      state = ON; 
      break;
    case ON:
      for(int i = 0; i < 3; i++){
        led[i]->switchOff();
      }
      state = OFF;
      break;
  }
}
