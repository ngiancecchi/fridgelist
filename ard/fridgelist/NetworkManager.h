#ifndef ____NetworkManager__
#define ____NetworkManager__

#include "Defines.h"
#include <WiFi.h>
#include <WiFiClient.h>
#include <WiFiServer.h>
#include <WiFiUdp.h>

#include "WiFiSettings.h"

#define RESP_SIZE 255

class NetworkManager {

private:
    int radioStatus;
    WiFiSettings* settings;
    WiFiClient client;
    String response;
    boolean canReadResponse;
    void disconnectFromServer();
    boolean makeRequest(String endpoint, String param, String value);
  
public:
    NetworkManager(WiFiSettings* st);
    boolean tryConnection();
    String receiveResponse();
    boolean deleteCurrentList();
    boolean sendTag(String tagName);
  };

#endif /* defined(____NetworkManager__) */
