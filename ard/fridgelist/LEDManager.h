#ifndef ____LEDManager__
#define ____LEDManager__

#include "Defines.h"
#include "Scheduler.h"
//                           R-G-B-
const int COLOR_GREEN[3] = {0,255,0};
const int COLOR_BLUE[3] = {0,0,255};
const int COLOR_YELLOW[3] = {255,255,0};
const int COLOR_CYAN[3] = {0,255,255};
const int COLOR_RED[3] = {255,0,0};

#define BLUE_COMP 5
#define GREEN_COMP 6
#define RED_COMP 9

class LEDManager {
    
private:
    int redQty;
    int blueQty;
    int greenQty;
    void setColor(int red, int green, int blue);
    Scheduler scheduler;
public:
    LEDManager();
    Scheduler getScheduler();
    void setMulticolorLed(const int color[3]);
    void scheduleBlink(const int color[3]);
    void setDefault();
    void manageServerResponse(String response);
};


#endif
