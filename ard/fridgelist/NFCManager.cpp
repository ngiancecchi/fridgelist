#include "NFCManager.h"

Adafruit_PN532 nfcBoard(IRQ, RESET);
uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID

NFCManager::NFCManager(){
}

bool NFCManager::setupBoard(){
  nfcBoard.begin();
  uint32_t versiondata = nfcBoard.getFirmwareVersion();
  if (! versiondata) {
    return false;
  }
  nfcBoard.SAMConfig();
  return true;
}

String NFCManager::readTag(){
  success = nfcBoard.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);
  if (success==1) {
    #if defined(ISDEBUG)
    nfcBoard.PrintHex(uid, uidLength);
    #endif
    return this->hexCode(uid,uidLength);
  }
  return "";
}

String NFCManager::hexCode (const byte * data, const uint32_t numBytes){
  uint32_t szPos;
  String returning = "";
  for (szPos=0; szPos < numBytes; szPos++) 
  {
    // Append leading 0 for small values
    returning = String(data[szPos], HEX) + returning;
    if (data[szPos] <= 0xF)
      returning = "0" + returning;
  }
  return returning;
}

