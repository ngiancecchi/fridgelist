#include "LEDManager.h"
#include "MulticolorBlinkTask.h"
MulticolorBlinkTask* blinkTask;

LEDManager::LEDManager(){
  pinMode(RED_COMP, OUTPUT);
  pinMode(GREEN_COMP, OUTPUT);
  pinMode(BLUE_COMP, OUTPUT);
  scheduler.init(200);
}

Scheduler LEDManager::getScheduler(){
  return this->scheduler; 
}

void LEDManager::setMulticolorLed(const int color[3]){
  this->redQty = color[0];
  this->greenQty = color[1];
  this->blueQty = color[2];
    
  analogWrite(BLUE_COMP,this->blueQty);
  analogWrite(RED_COMP,this->redQty);
  analogWrite(GREEN_COMP,this->greenQty);
  scheduler.removeLastTask();
}

void LEDManager::scheduleBlink(const int color[3]){
  this->setMulticolorLed(color);
  if(!blinkTask){
    blinkTask = new MulticolorBlinkTask(RED_COMP, GREEN_COMP, BLUE_COMP);
    blinkTask->init(200); 
  }
  blinkTask->setColorComponents(color);
  scheduler.addTask(blinkTask);
}

void LEDManager::setDefault(){
  this->setMulticolorLed(COLOR_BLUE);
}

void LEDManager::manageServerResponse(String response){
  if(response.equals("0")){
     this->setMulticolorLed(COLOR_RED);
  } else if(response.equals("1")){
     this->setMulticolorLed(COLOR_GREEN);
  } else if(response.equals("2")){
     this->setMulticolorLed(COLOR_CYAN);
  } else {
    this->setMulticolorLed(COLOR_RED);
  }
}

