#include "Led.h"
#include "Arduino.h"

Led::Led(int pin, int colorLevel){
  this->pin = pin;
  this->colorLevel = colorLevel;
  pinMode(pin,OUTPUT);
}

void Led::switchOn(){
  analogWrite(pin,colorLevel);
}

void Led::switchOff(){
  analogWrite(pin,0);
};
