#include "NetworkManager.h"

char hostname[] = HOSTNAME;
char baseurl[] = BASEURL;

NetworkManager::NetworkManager(WiFiSettings* st){
  this->radioStatus = WL_IDLE_STATUS;
  this->settings = st;
  canReadResponse = false;
}

boolean NetworkManager::tryConnection(){
  if(WiFi.status() == WL_NO_SHIELD){
    #if defined(ISDEBUG)
    Serial.println(F("[WiFi] shield not present"));
    #endif
    return false;
  }
  
  while(radioStatus != WL_CONNECTED){
    #if defined(ISDEBUG)
    Serial.println(F("[WiFi] Attempting to connect to SSID: "));
    Serial.println(this->settings->getSSID());
    #endif
    
    char ssidBuffer[SSID_MAX_LENGTH];
    char pswBuffer[PSW_MAX_LENGTH];
    this->settings->getSSID().toCharArray(ssidBuffer,SSID_MAX_LENGTH);
    this->settings->getPassword().toCharArray(pswBuffer,PSW_MAX_LENGTH);
    
    this->radioStatus = WiFi.begin(ssidBuffer,pswBuffer);
    delay(1000);
    
    if(this->radioStatus == WL_NO_SSID_AVAIL){
      #if defined(ISDEBUG)
      Serial.println(F("[WiFi] Can't connect to the network due to SSID not available"));
      #endif
      return false;
    }
    
    if(this->radioStatus == WL_CONNECT_FAILED){
      #if defined(ISDEBUG)
      Serial.println(F("[WiFi] Can't connect due to a connection fail"));
      #endif
      return false;
    }
    
    if(this->radioStatus == WL_CONNECTION_LOST){
      #if defined(ISDEBUG)
      Serial.println(F("[WiFi] Can't connect. Connection lost."));
      #endif
      return false;   
    } 
    
  }
  
  #if defined(ISDEBUG)
  Serial.print("[WiFi] You're connected to the network!");
  #endif
  
  return true;
}

boolean NetworkManager::deleteCurrentList(){ 
  return this->makeRequest("reset.php", "", "");
}

boolean NetworkManager::sendTag(String tagName){
  return this->makeRequest("add_tag.php", "tid", tagName);
}

boolean NetworkManager::makeRequest(String endpoint, String param, String value){
  disconnectFromServer();
  response = ""; 
  
  #if defined(ISDEBUG)
  Serial.println(F("\n[WIFI] Starting connection to server..."));
  #endif
  boolean result = false;
  if (client.connect(hostname, 80)) {
    #if defined(ISDEBUG)
    Serial.println(F("[WIFI] connected to server"));
    #endif
    // Make a HTTP request:
    client.print("GET ");
    client.print(baseurl);
    client.print(endpoint);
    if(!param.equals("")){
      client.print("?");
      client.print(param);
      client.print("=");
      client.print(value);
    }
    client.print(" HTTP/1.1\n");
    client.print("Host: ");
    client.print(hostname);
    client.print("\n");
    client.println("Connection: close");
    client.println();
    result=true;
  } 
  return result;
}

String NetworkManager::receiveResponse(){

  while (client.available()) {
    char c = client.read(); 
    if(c == '}'){ //end of response
      canReadResponse=false;
      return response;
    }
    if(canReadResponse) //append response
      response += c;
    if(c == '{') //start of response
      canReadResponse=true;   
  }
  return "";
}

void NetworkManager::disconnectFromServer(){
  if(!client.connected()){
    //client.flush();
    client.stop();
    #if defined(ISDEBUG)
    Serial.println(F("Disconnected from server"));
    #endif
  }
}
