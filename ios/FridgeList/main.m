//
//  main.m
//  FridgeList
//
//  Created by Nicola Giancecchi on 23/08/14.
//  Copyright (c) 2014 FridgeList. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FLAppDelegate class]));
    }
}
