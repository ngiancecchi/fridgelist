//
//  NSArray+JSONKit.m
//  FridgeList
//
//  Created by Nicola Giancecchi on 23/08/14.
//  Copyright (c) 2014 FridgeList. All rights reserved.
//

#import "NSArray+JSONKit.h"

@implementation NSArray (JSONKit)

-(NSData*)JSONData{
	return [NSJSONSerialization dataWithJSONObject:self options:kNilOptions error:nil];
}

-(NSString*)JSONString{
	return [[NSString alloc] initWithData:[self JSONData] encoding:NSUTF8StringEncoding];
}

@end
