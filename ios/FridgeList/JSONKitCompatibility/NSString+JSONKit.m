//
//  NSString+JSONKit.m
//  FridgeList
//
//  Created by Nicola Giancecchi on 23/08/14.
//  Copyright (c) 2014 FridgeList. All rights reserved.
//

#import "NSString+JSONKit.h"
#import "NSData+JSONKit.h"

@implementation NSString (JSONKit)

-(id)objectFromJSONString{
	return  [[self dataUsingEncoding:NSUTF8StringEncoding] objectFromJSONData];
}

@end
