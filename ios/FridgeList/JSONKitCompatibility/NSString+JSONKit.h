//
//  NSString+JSONKit.h
//  FridgeList
//
//  Created by Nicola Giancecchi on 23/08/14.
//  Copyright (c) 2014 FridgeList. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (JSONKit)

-(id)objectFromJSONString;

@end
