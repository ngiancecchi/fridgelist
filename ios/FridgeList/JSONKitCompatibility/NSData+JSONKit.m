//
//  NSData+JSONKit.m
//  FridgeList
//
//  Created by Nicola Giancecchi on 23/08/14.
//  Copyright (c) 2014 FridgeList. All rights reserved.
//

#import "NSData+JSONKit.h"

@implementation NSData (JSONKit)

-(id)objectFromJSONData{

	return  [NSJSONSerialization
			 JSONObjectWithData:self
			 options:kNilOptions
			 error:nil];
}

@end
