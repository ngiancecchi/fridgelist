//
//  JSONKitCompatibility.h
//  FridgeList
//
//  Created by Nicola Giancecchi on 23/08/14.
//  Copyright (c) 2014 FridgeList. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+JSONKit.h"
#import "NSData+JSONKit.h"
#import "NSDictionary+JSONKit.h"
#import "NSArray+JSONKit.h"
