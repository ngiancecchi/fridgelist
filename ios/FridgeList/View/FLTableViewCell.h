//
//  FLTableViewCell.h
//  FridgeList
//
//  Created by Nicola Giancecchi on 20/03/15.
//  Copyright (c) 2015 FridgeList. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgProduct;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

@end
