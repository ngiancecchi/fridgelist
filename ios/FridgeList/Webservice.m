//
//  Webservice.m
//  FridgeList
//
//  Created by Nicola Giancecchi on 23/08/14.
//  Copyright (c) 2014 FridgeList. All rights reserved.
//

#import "Webservice.h"
#import "JSONKitCompatibility.h"
#import "Tag.h"


@implementation Webservice


static Webservice * _webservice;

+ (Webservice *)sharedWebservice {
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_webservice = [[Webservice alloc] init];
	});
	return _webservice;
}


-(void)getProductsAndDone:(DoneArrayBlock)done{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager GET:[@URL stringByAppendingFormat:@"/get_products.php"] parameters:nil success:^(AFHTTPRequestOperation *operation, NSArray *responseObject) {
        
        if(responseObject){
            
            //DBContext *ctx = [DBContext new];
            NSMutableArray *resp = [NSMutableArray array];
            
            for(NSDictionary *dict in responseObject){
                Tag *tag = [Tag new];
                tag.Id = dict[@"idTag"];
                tag.name = dict[@"name"];
                tag.date = [NSDate dateWithTimeIntervalSince1970:[dict[@"date"] longLongValue]];
                tag.imageURL = dict[@"image"];
                [resp addObject:tag];
                //[tag commit];
                //[ctx addEntityToContext:tag];
            }
            //[ctx commit];
            done(YES,resp);
        } else {
            done(NO,nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        done(NO,nil);
    }];
}

-(void)resetListAndDone:(DoneBlock)done{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:[@URL stringByAppendingFormat:@"/reset.php"] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *response = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        if(response){
            if([response isEqual:@"{1}"]){
                done(YES);
            } else {
                done(NO);
            }
        } else {
            done(NO);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        done(NO);
    }];
}

-(void)deleteItemFromList:(DoneBlock)done{
    
}

@end
