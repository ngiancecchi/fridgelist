//
//  Webservice.h
//  FridgeList
//
//  Created by Nicola Giancecchi on 23/08/14.
//  Copyright (c) 2014 FridgeList. All rights reserved.
//
#import <AFNetworking.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <AFNetworking/UIButton+AFNetworking.h>

#define URL "http://nicola.giancecchi.com/dev/fridgelist/api"



typedef void (^DoneBlock)(BOOL completed);
typedef void (^DoneArrayBlock)(BOOL completed, NSArray *array);
typedef void (^DoneStringBlock)(BOOL completed,NSString* response);
typedef void (^DoneImageBlock)(BOOL completed,UIImage* image,NSString* errorMessage);
typedef void (^DoneDownloadBlock)(BOOL completed, NSString* localFilePath);
typedef void (^ProgressChangedBlock)(double prog);


@interface Webservice : NSObject{
    NSMutableArray *downloadQueue;
}

+ (Webservice *)sharedWebservice;

-(void)getProductsAndDone:(DoneArrayBlock)done;
-(void)resetListAndDone:(DoneBlock)done;
-(void)deleteItemFromList:(DoneBlock)done;


@end
