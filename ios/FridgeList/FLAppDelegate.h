//
//  FLAppDelegate.h
//  FridgeList
//
//  Created by Nicola Giancecchi on 23/08/14.
//  Copyright (c) 2014 FridgeList. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLAppDelegate : UIResponder <UIApplicationDelegate, DBDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
