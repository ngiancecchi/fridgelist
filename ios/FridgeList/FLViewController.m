//
//  FLViewController.m
//  FridgeList
//
//  Created by Nicola Giancecchi on 23/08/14.
//  Copyright (c) 2014 FridgeList. All rights reserved.
//

#import "FLViewController.h"
#import "FLTableViewCell.h"

@interface FLViewController ()

@end

@implementation FLViewController

static NSString *identifier = @"CellIdentifier";

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:identifier];
    [self loadData:nil];
    
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    [refresh addTarget:self action:@selector(loadData:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refresh];
    /*
    UIBarButtonItem *bbi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(callDelete:)];
    bbi.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = bbi;*/
    
    NSTimer *timer = [NSTimer timerWithTimeInterval:10.0 target:self selector:@selector(loadData:) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"FLTableViewCell" bundle:nil] forCellReuseIdentifier:identifier];
}

-(void)loadData:(UIRefreshControl*)refresh{
    
    [[Webservice sharedWebservice] getProductsAndDone:^(BOOL completed, NSArray *array) {
        if(completed){
            self.elements = array;
            [self.tableView reloadData];
            if([refresh isKindOfClass:[UIRefreshControl class]]){
                [refresh endRefreshing];
            }
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.elements.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FLTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    Tag *t = self.elements[indexPath.row];
    cell.lblTitle.text = t.name;
    cell.lblDescription.text = [NSString stringWithFormat:@"Added %@", t.date];
    [cell.imgProduct setImageWithURL:[NSURL URLWithString:t.imageURL]];
    cell.imgProduct.layer.cornerRadius = cell.imgProduct.frame.size.width/2;
    cell.imgProduct.layer.masksToBounds  = YES;
    cell.imgProduct.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.imgProduct.layer.borderWidth = 1.0;
    
    return cell;
}

- (IBAction)callDelete:(UIBarButtonItem *)sender {
    [[Webservice sharedWebservice] resetListAndDone:^(BOOL completed) {
        if(completed){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete" message:@"Items deleted successfully" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alert show];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"There was an error during the operation." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alert show];
        }
        [self loadData:nil];
        
    }];
}

@end
