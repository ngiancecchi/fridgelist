//
//  FLViewController.h
//  FridgeList
//
//  Created by Nicola Giancecchi on 23/08/14.
//  Copyright (c) 2014 FridgeList. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tag.h"

@interface FLViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *elements;
@end
