//
//  Tag.m
//  FridgeList
//
//  Created by Nicola Giancecchi on 24/08/14.
//  Copyright (c) 2014 FridgeList. All rights reserved.
//

#import "Tag.h"

@implementation Tag

@dynamic name, date;

@end
