//
//  Tag.h
//  FridgeList
//
//  Created by Nicola Giancecchi on 24/08/14.
//  Copyright (c) 2014 FridgeList. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Tag : DBObject

@property(strong) NSString *name;
@property(strong) NSDate *date;
@property(strong) NSString *imageURL;

@end
