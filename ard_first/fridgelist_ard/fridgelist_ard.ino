/*

    PLEASE NOTE THAT THIS SKETCH WORKS ONLY
    ON ARDUINO IDE 1.0.2

*/


#include "Adafruit_PN532.h"
#include <WiFi.h>
#include <SPI.h>

#define SCK  (2)
#define MOSI (3)
#define SS   (4)
#define MISO (5)

char networkSSID[] = "Giancecchi-Private";    // WiFi network name (SSID)
char password[] = "T0rr4cc14";         // WiFi network password (WPA/WEP)


char baseURL[] = "nicola.giancecchi.com";
//base URL of the add_tag route
int keyIndex = 0;                           // network index number (only for WEP)
int status = WL_IDLE_STATUS;                // WiFi radio Status
WiFiClient client;

Adafruit_PN532 nfc(SCK, MISO, MOSI, SS);

void setup(void) {
  Serial.begin(9600);
  Serial.println("Hello!");

  nfc.begin();
  
  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    Serial.print("[NFC ] Didn't find PN53x board");
    while (1); // halt
  }
  
  
  // check for the presence of the WiFi shield:
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("[WIFI] WiFi shield not present"); 
    // don't continue:
    while(true);
  } 
  
 // attempt to connect to Wifi network:
  while ( status != WL_CONNECTED) { 
    Serial.print("[WIFI] Attempting to connect to SSID: ");
    Serial.println(networkSSID);
    status = WiFi.begin(networkSSID, password);

    // wait 1 seconds for connection:
    delay(1000);
  }
  
  // Got ok data, print it out!
  Serial.print("[NFC ] Found chip PN5"); Serial.println((versiondata>>24) & 0xFF, HEX); 
  Serial.print("[NFC ] Firmware ver. "); Serial.print((versiondata>>16) & 0xFF, DEC); 
  Serial.print('.'); Serial.println((versiondata>>8) & 0xFF, DEC);
  
  // configure board to read RFID tags
  nfc.SAMConfig();
  
  
  // you're connected now, so print out the data:
  Serial.print("[WIFI] You're connected to the network");
  printCurrentNet();
  printWifiData();
  
  Serial.println("[NFC ] Waiting for an ISO14443A Card ...");
}


void loop(void) {
  uint8_t success;
  uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
  uint8_t uidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
    
  // Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
  // 'uid' will be populated with the UID, and uidLength will indicate
  // if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);
  
  if (success) {
    // Display some basic information about the card
    Serial.println("[NFC ] Found an ISO14443A card");
    Serial.print("  UID Length: ");Serial.print(uidLength, DEC);Serial.println(" bytes");
    Serial.print("  UID Value: ");
    nfc.PrintHex(uid, uidLength);
    Serial.println("");
    String identifier = hexCode(uid,uidLength);
    Serial.print("handmade hex: ");
    Serial.println(identifier);
    sendNFCtoServer(identifier);
    
  
    /*
    if (uidLength == 4)
    {
      // We probably have a Mifare Classic card ... 
      Serial.println("[NFC ] Seems to be a Mifare Classic card (4 byte UID)");
	  
      // Now we need to try to authenticate it for read/write access
      // Try with the factory default KeyA: 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF
      Serial.println("[NFC ] Trying to authenticate block 4 with default KEYA value");
      uint8_t keya[6] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
	  
	  // Start with block 4 (the first block of sector 1) since sector 0
	  // contains the manufacturer data and it's probably better just
	  // to leave it alone unless you know what you're doing
      success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength, 4, 0, keya);
	  
      if (success)
      {
        Serial.println("[NFC ] Sector 1 (Blocks 4..7) has been authenticated");
        uint8_t data[16];
		
        // If you want to write something to block 4 to test with, uncomment
		// the following line and this text should be read back in a minute
        // data = { 'a', 'd', 'a', 'f', 'r', 'u', 'i', 't', '.', 'c', 'o', 'm', 0, 0, 0, 0};
        // success = nfc.mifareclassic_WriteDataBlock (4, data);

        // Try to read the contents of block 4
        success = nfc.mifareclassic_ReadDataBlock(4, data);
		
        if (success)
        {
          // Data seems to have been read ... spit it out
          Serial.println("[NFC ] Reading Block 4:");
          nfc.PrintHexChar(data, 16);
          Serial.println("");
		  
          // Wait a bit before reading the card again
          delay(1000);
        }
        else
        {
          Serial.println("[NFC ] Ooops ... unable to read the requested block.  Try another key?");
        }
      }
      else
      {
        Serial.println("[NFC ] Ooops ... authentication failed: Try another key?");
      }
    }
    
    if (uidLength == 7)
    {
      // We probably have a Mifare Ultralight card ...
      Serial.println("[NFC ] Seems to be a Mifare Ultralight tag (7 byte UID)");
	  
      // Try to read the first general-purpose user page (#4)
      Serial.println("[NFC ]Reading page 4");
      uint8_t data[32];
      success = nfc.mifareultralight_ReadPage (4, data);
      if (success)
      {
        // Data seems to have been read ... spit it out
        nfc.PrintHexChar(data, 4);
        Serial.println("");
		
        // Wait a bit before reading the card again
        delay(1000);
      }
      else
      {
        Serial.println("[NFC ] Ooops ... unable to read the requested page!?");
      }
    }*/
  }
}

void sendNFCtoServer(String identifier){
 
  Serial.println("\n[WIFI] Starting connection to server...");
  // if you get a connection, report back via serial:
  if (client.connect(baseURL, 80)) {
    Serial.println("[WIFI] connected to server");
    // Make a HTTP request:
    client.print("GET /dev/fridgelist/api/add_tag.php?tid=");
    client.print(identifier);
    client.print(" HTTP/1.1\n");
    client.print("Host: ");
    client.print(baseURL);
    client.print("\n");
    client.println("Connection: close");
    client.println();
  } 
  
}


String hexCode (const byte * data, const uint32_t numBytes)
{

  uint32_t szPos;
  String returning = "";
  for (szPos=0; szPos < numBytes; szPos++) 
  {
    // Append leading 0 for small values
    returning = String(data[szPos], HEX) + returning;
    if (data[szPos] <= 0xF)
      returning = "0" + returning;
  }
  return returning;
}



void printWifiData() {
  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
    Serial.print("IP Address: ");
  Serial.println(ip);
  Serial.println(ip);
  
  // print your MAC address:
  byte mac[6];  
  WiFi.macAddress(mac);
  Serial.print("MAC address: ");
  Serial.print(mac[5],HEX);
  Serial.print(":");
  Serial.print(mac[4],HEX);
  Serial.print(":");
  Serial.print(mac[3],HEX);
  Serial.print(":");
  Serial.print(mac[2],HEX);
  Serial.print(":");
  Serial.print(mac[1],HEX);
  Serial.print(":");
  Serial.println(mac[0],HEX);
  
  // print your subnet mask:
  IPAddress subnet = WiFi.subnetMask();
  Serial.print("NetMask: ");
  Serial.println(subnet);

  // print your gateway address:
  IPAddress gateway = WiFi.gatewayIP();
  Serial.print("Gateway: ");
  Serial.println(gateway);
}

void printCurrentNet() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print the MAC address of the router you're attached to:
  byte bssid[6];
  WiFi.BSSID(bssid);    
  Serial.print("BSSID: ");
  Serial.print(bssid[5],HEX);
  Serial.print(":");
  Serial.print(bssid[4],HEX);
  Serial.print(":");
  Serial.print(bssid[3],HEX);
  Serial.print(":");
  Serial.print(bssid[2],HEX);
  Serial.print(":");
  Serial.print(bssid[1],HEX);
  Serial.print(":");
  Serial.println(bssid[0],HEX);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.println(rssi);

  // print the encryption type:
  byte encryption = WiFi.encryptionType();
  Serial.print("Encryption Type:");
  Serial.println(encryption,HEX);
}



